package main

import (
	"flag"
	"log"
)

var dryRunFlag = flag.Bool("dry-run", false, "Runs without making any modifications in RabbitMQ")

func main() {
	flag.Parse()
	sync := &Sync{
		DryRun: *dryRunFlag,
	}
	if err := sync.Init(); err != nil {
		log.Fatal(err)
	}
	if err := sync.Run(); err != nil {
		log.Fatal(err)
	}
	log.Println("done.")
}
