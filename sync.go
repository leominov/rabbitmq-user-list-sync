package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/michaelklishin/rabbit-hole"
)

var (
	DefaultRabbitMQPermissions = rabbithole.Permissions{
		Configure: ".*",
		Write:     ".*",
		Read:      ".*",
	}
)

type Sync struct {
	DryRun   bool
	cfg      *Config
	vaultCli *VaultClient
	users    []*User
	rmqCli   *rabbithole.Client
}

type User struct {
	Name     string
	Password string
	Tags     string
}

func (s *Sync) Init() error {
	if s.DryRun {
		log.Println("[!] Dry-run mode enabled")
	}
	cfg, err := LoadConfig()
	if err != nil {
		return err
	}
	s.cfg = cfg
	vaultCli, err := NewVaultClient(cfg.VaultAddr, cfg.VaultKVVersion, cfg.VaultLogin, cfg.VaultPassword, cfg.VaultToken, cfg.VaultMethod, http.DefaultClient)
	if err != nil {
		return err
	}
	s.vaultCli = vaultCli
	userPath := fmt.Sprintf("%s/%s", cfg.RabbitMQUserListPath, cfg.RabbitMQLogin)
	data, err := s.vaultCli.GetPath(userPath)
	if err != nil {
		return fmt.Errorf("Can't read user %s data: %v", cfg.RabbitMQLogin, err)
	}
	pwd, ok := data[cfg.RabbitMQPasswordKey]
	if !ok {
		return fmt.Errorf("User %s password is not specified", cfg.RabbitMQLogin)
	}
	rmqCli, err := rabbithole.NewClient(cfg.RabbitMQAddr, cfg.RabbitMQLogin, pwd)
	if err != nil {
		return err
	}
	if _, err := rmqCli.Whoami(); err != nil {
		return fmt.Errorf("Can't get whoami information: %v", err)
	}
	s.rmqCli = rmqCli
	return nil
}

func (s *Sync) LoadUsers() error {
	users, err := s.vaultCli.ListPath(s.cfg.RabbitMQUserListPath)
	if err != nil {
		return fmt.Errorf("Can't get list of users: %v", err)
	}
	for _, userName := range users {
		userPath := fmt.Sprintf("%s/%s", s.cfg.RabbitMQUserListPath, userName)
		data, err := s.vaultCli.GetPath(userPath)
		if err != nil {
			return fmt.Errorf("Can't read user %s data: %v", userName, err)
		}
		val, ok := data[s.cfg.RabbitMQSyncKey]
		if !ok {
			log.Printf("User %s skipped (sync value is not specified)", userName)
			continue
		}
		valBool, err := strconv.ParseBool(val)
		if err != nil {
			return fmt.Errorf("Can't parse user %s sync value: %v", userName, err)
		}
		if !valBool {
			log.Printf("User %s skipped (sync value)", userName)
			continue
		}
		pwd, ok := data[s.cfg.RabbitMQPasswordKey]
		if !ok {
			log.Printf("User %s skipped (password value is not specified)", userName)
			continue
		}
		tags, ok := data[s.cfg.RabbitMQTagsKey]
		if !ok {
			log.Printf("User %s skipped (tags value is not specified)", userName)
			continue
		}
		log.Printf("User %s added to sync", userName)
		user := &User{
			Name:     userName,
			Password: pwd,
			Tags:     tags,
		}
		s.users = append(s.users, user)
	}
	return nil
}

func (s *Sync) AddUser(user *User) error {
	userSettings := rabbithole.UserSettings{
		Password: user.Password,
		Tags:     user.Tags,
	}
	log.Printf("Adding user %s...", user.Name)
	if !s.DryRun {
		if _, err := s.rmqCli.PutUser(user.Name, userSettings); err != nil {
			return err
		}
	}
	log.Printf("Setting up user %s permissions...", user.Name)
	if !s.DryRun {
		if _, err := s.rmqCli.UpdatePermissionsIn("/", user.Name, DefaultRabbitMQPermissions); err != nil {
			return err
		}
	}
	return nil
}

func (s *Sync) AddUsers(users []*User) error {
	for _, user := range users {
		err := s.AddUser(user)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Sync) Run() error {
	err := s.LoadUsers()
	if err != nil {
		return err
	}
	var usersToAdd []*User
	for _, user := range s.users {
		_, err := s.rmqCli.GetUser(user.Name)
		if err == nil {
			continue
		}
		if strings.Contains(err.Error(), "Not Found") {
			usersToAdd = append(usersToAdd, user)
			continue
		}
		return fmt.Errorf("Can't get user %s information: %v", user.Name, err)
	}
	if len(usersToAdd) == 0 {
		log.Println("Nothing to do")
		return nil
	}
	if err := s.AddUsers(usersToAdd); err != nil {
		return err
	}
	return nil
}
