package main

import (
	"fmt"
	"os"
	"strings"
)

const (
	defaultRabbitMQPasswordKey = "password"
	defaultRabbitMQSyncKey     = "sync"
	defaultRabbitMQLogin       = "admin"
	defaultRabbitMQTagsKey     = "tags"
)

var (
	allowedKV = map[string]bool{
		"1": true,
		"2": true,
	}
)

type Config struct {
	VaultToken           string
	VaultLogin           string
	VaultPassword        string
	VaultMethod          string
	VaultAddr            string
	VaultKVVersion       string
	RabbitMQUserListPath string
	RabbitMQPasswordKey  string
	RabbitMQSyncKey      string
	RabbitMQTagsKey      string
	RabbitMQLogin        string
	RabbitMQAddr         string
}

func LoadConfig() (*Config, error) {
	var (
		errs          []string
		vaultToken    string
		vaultLogin    string
		vaultPassword string
	)
	vaultMethod := strings.ToLower(os.Getenv("VAULT_METHOD"))
	if len(vaultMethod) == 0 {
		errs = append(errs, "Vault Auth Method (VAULT_METHOD)")
	}
	if vaultMethod == "token" {
		vaultToken = os.Getenv("VAULT_TOKEN")
		if len(vaultToken) == 0 {
			errs = append(errs, "Vault Token (VAULT_TOKEN)")
		}
	} else {
		vaultLogin = os.Getenv("VAULT_LOGIN")
		if len(vaultLogin) == 0 {
			errs = append(errs, "Vault Login (VAULT_LOGIN)")
		}
		vaultPassword = os.Getenv("VAULT_PASSWORD")
		if len(vaultPassword) == 0 {
			errs = append(errs, "Vault Password (VAULT_PASSWORD)")
		}
	}
	vaultAddr := os.Getenv("VAULT_ADDR")
	if len(vaultAddr) == 0 {
		errs = append(errs, "Vault Address (VAULT_ADDR)")
	}
	vaultKVVersion := os.Getenv("VAULT_KV_VERSION")
	if len(vaultKVVersion) == 0 {
		errs = append(errs, "Vault KV Version (VAULT_KV_VERSION)")
	} else {
		if _, ok := allowedKV[vaultKVVersion]; !ok {
			return nil, fmt.Errorf("Incorrect Vault KV version: %s", vaultKVVersion)
		}
	}
	rabbitMQUserListPath := os.Getenv("RABBITMQ_USER_LIST_PATH")
	if len(rabbitMQUserListPath) == 0 {
		errs = append(errs, "RabbitMQ User List Path (RABBITMQ_USER_LIST_PATH)")
	}
	rabbitMQPasswordKey := os.Getenv("RABBITMQ_PASSWORD_KEY")
	if len(rabbitMQPasswordKey) == 0 {
		rabbitMQPasswordKey = defaultRabbitMQPasswordKey
	}
	rabbitMQSyncKey := os.Getenv("RABBITMQ_SYNC_KEY")
	if len(rabbitMQSyncKey) == 0 {
		rabbitMQSyncKey = defaultRabbitMQSyncKey
	}
	rabbitMQTagsKey := os.Getenv("RABBITMQ_TAGS_KEY")
	if len(rabbitMQTagsKey) == 0 {
		rabbitMQTagsKey = defaultRabbitMQTagsKey
	}
	rabbitMQLogin := os.Getenv("RABBITMQ_LOGIN")
	if len(rabbitMQLogin) == 0 {
		rabbitMQLogin = defaultRabbitMQLogin
	}
	rabbitMQAddr := os.Getenv("RABBITMQ_ADDR")
	if len(rabbitMQAddr) == 0 {
		errs = append(errs, "RabbitMQ Address (RABBITMQ_ADDR)")
	}
	if len(errs) != 0 {
		return nil, fmt.Errorf("Variables must be specified: %s", strings.Join(errs, ", "))
	}
	return &Config{
		VaultToken:           vaultToken,
		VaultLogin:           vaultLogin,
		VaultPassword:        vaultPassword,
		VaultMethod:          vaultMethod,
		VaultAddr:            vaultAddr,
		VaultKVVersion:       vaultKVVersion,
		RabbitMQUserListPath: rabbitMQUserListPath,
		RabbitMQPasswordKey:  rabbitMQPasswordKey,
		RabbitMQSyncKey:      rabbitMQSyncKey,
		RabbitMQTagsKey:      rabbitMQTagsKey,
		RabbitMQLogin:        rabbitMQLogin,
		RabbitMQAddr:         rabbitMQAddr,
	}, nil
}
