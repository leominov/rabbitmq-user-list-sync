FROM golang:1.11.4 as builder
WORKDIR /go/src/github.com/leominov/rabbitmq-user-list-sync
COPY . .
RUN CGO_ENABLED=0 go build -o rabbitmq-user-list-sync

FROM scratch
COPY --from=builder /go/src/github.com/leominov/rabbitmq-user-list-sync/rabbitmq-user-list-sync /go/bin/rabbitmq-user-list-sync
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["/go/bin/rabbitmq-user-list-sync"]
