# RabbitMQ User List Sync

Works in couple with one and only HashiCorp Vault.

## Required environment variables

* `VAULT_METHOD` (`ldap`, `userpass` or `token`)
* `VAULT_LOGIN` (required for `ldap` and `userpass`)
* `VAULT_PASSWORD` (required for `ldap` and `userpass`)
* `VAULT_TOKEN` (required for `token`)
* `VAULT_ADDR`
* `VAULT_KV_VERSION` (`1` or `2`)
* `RABBITMQ_USER_LIST_PATH`
* `RABBITMQ_PASSWORD_KEY` (`password` by default)
* `RABBITMQ_SYNC_KEY` (`sync` by default)
* `RABBITMQ_TAGS_KEY` (`tags` by default)
* `RABBITMQ_LOGIN` (`admin` by default)
* `RABBITMQ_ADDR`
