package main

import (
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"strings"

	"github.com/hashicorp/vault/api"
)

type VaultClient struct {
	kvVersion string
	client    *api.Client
}

func NewVaultClient(addr, kvVersion, user, pass, token, authMethod string, httpCli *http.Client) (*VaultClient, error) {
	config := api.Config{
		Address:    addr,
		HttpClient: httpCli,
	}
	client, err := api.NewClient(&config)
	if err != nil {
		return nil, err
	}
	if authMethod == "token" {
		client.SetToken(token)
	} else {
		options := map[string]interface{}{
			"password": pass,
		}
		path := fmt.Sprintf("auth/%s/login/%s", authMethod, user)
		secret, err := client.Logical().Write(path, options)
		if err != nil {
			return nil, err
		}
		client.SetToken(secret.Auth.ClientToken)
	}
	return &VaultClient{
		kvVersion: kvVersion,
		client:    client,
	}, nil
}

func (v *VaultClient) ListPath(path string) ([]string, error) {
	var result []string
	if v.kvVersion != "1" {
		path = addVersionizedFolder(path, "metadata")
	}
	secret, err := v.client.Logical().List(path)
	if err != nil {
		return result, err
	}
	if secret == nil || secret.Data == nil {
		return result, fmt.Errorf("Vault results empty for '%s'", path)
	}
	val, ok := secret.Data["keys"]
	if !ok {
		return result, errors.New("Not found")
	}
	vOf := reflect.ValueOf(val)
	for i := 0; i < vOf.Len(); i++ {
		strct := vOf.Index(i).Interface()
		result = append(result, strct.(string))
	}

	return result, nil
}

func addVersionizedFolder(path, name string) string {
	data := strings.SplitN(path, "/", 2)
	if len(data) != 2 {
		return path
	}
	resultMap := []string{data[0], name, data[1]}
	resultString := strings.Join(resultMap, "/")
	return resultString
}

func (v *VaultClient) GetPath(path string) (map[string]string, error) {
	result := make(map[string]string)
	if v.kvVersion != "1" {
		path = addVersionizedFolder(path, "data")
	}
	secret, err := v.client.Logical().Read(path)
	if err != nil {
		return result, err
	}
	if secret == nil || secret.Data == nil {
		return result, fmt.Errorf("Vault results empty secret for '%s'", path)
	}
	var vOf reflect.Value
	if v.kvVersion != "1" {
		val, ok := secret.Data["data"]
		if !ok {
			return result, fmt.Errorf("Vault results empty secret v2-data for '%s'", path)
		}
		vOf = reflect.ValueOf(val)
	} else {
		vOf = reflect.ValueOf(secret.Data)
	}
	keys := vOf.MapKeys()
	for _, key := range keys {
		keyStr := key.String()
		result[keyStr] = fmt.Sprintf("%s", vOf.MapIndex(key))
	}
	return result, nil
}
